#include <iostream>
#include <fstream>
#include <cstring>
#include <random>
#include <clocale>
#include <array>
#include <ncursesw/ncurses.h>

int kolumny = 0;
int rzedy = 0;
// Print podkreślników
void printUnderlines(int length){
    for(int i = 0; i < length; i++){
        printw("_");
    }
}
// pomocnicze drukowanie odpowiedzi (finalnie tego nie będzie)
void printWord(int numberOfChars, std::string randomWord){
    // string to char
    char guessingWord[numberOfChars + 1];
    strcpy(guessingWord, randomWord.c_str());
    printw(guessingWord);
}

//Mechanizm gry
int printGuesses(int numberOfChars, std::string randomWord) {
    int lives = 5;
    int changes = 0;
    char c;

    std::string placeForGuesses;
    for(int i = 0; i<numberOfChars; i++){
        placeForGuesses += " ";
    }
    noecho();
    do {
        c = getch();
        if(c == 'q'){
            move(3,30);
            printw("Quitted!");
            return 0;
        }
        move(15,30);
        int lastChange = changes;
        for(int i = 0; i<numberOfChars; i++){
            if(randomWord[i] == c){
                placeForGuesses[i]=c;
                changes+=1;
            }
        }
        if(lastChange == changes){
            lives-=1;
        }

        //char guessChar[numberOfChars];
        //strcpy(guessChar, placeForGuesses.c_str());
        printw(placeForGuesses.data());
        //printw("%d", numberOfChars);
        if(placeForGuesses.compare(randomWord)==0){
            move(3,30);
            printw("Congrats! Press any button to quit...");
            return 1;
        }

    } while(lives > 0);

    return lives;
}
//Pobieranie wyrazu z pliku
std::string getWord(std::string fileName){
    // Generating random number
    std::mt19937 engine;
    std::uniform_int_distribution<int> distribution(0, 3056759);
    engine.seed(std::random_device{}());
    auto const RANDOM_NUMBER = distribution(engine);

    // Getting a word from file
    std::ifstream f;
    std::string word;
    f.open(fileName, std::ios::in);
    int current_line = 0;
    while(std::getline(f,word))
    {
        if(current_line == RANDOM_NUMBER) {
            break;
        }
        current_line++;
    }
    f.close();
    return word;
}

int wordLength(std::string s){
    int strLen = s.length();
    unsigned int u = 0;
    const char *c_str = s.c_str();
    int charCount = 0;
    while(u < strLen)
    {
        u += mblen(&c_str[u], strLen - u);
        charCount += 1;
    }
    return charCount-1;
}




int main()
{
    setlocale(LC_ALL, "");
    initscr();
    std::string word = getWord("slowa.txt");
    int charCount = wordLength(word);
    getmaxyx(stdscr, rzedy, kolumny);
    move(18,30);
    printWord(charCount, word);
    move(16,30);
    printUnderlines(charCount);
    move(15,30);
    printGuesses(charCount, word);



    getch();
    endwin();
}
